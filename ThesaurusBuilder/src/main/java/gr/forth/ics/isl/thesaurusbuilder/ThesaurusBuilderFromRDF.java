package gr.forth.ics.isl.thesaurusbuilder;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.openrdf.model.URI;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.memory.MemoryStore;

/**
 * @author Yannis Marketakis (marketak 'at' ics 'dot' forth 'dot' gr)
 */
public class ThesaurusBuilderFromRDF {
    private static final String UNKNOWN_VALUE="unknown";
    private static final int TERM_THRESHOLD=3;
    private static final String ID_LABEL="id";
    private static final String NAME_LABEL="name";
    private static final String PERSON_SPARQL_QUERY="SELECT ?label "
                                             +" WHERE{?person <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://eurocris.org/ontology/cerif#Person>. "
                                             +" ?person <http://www.w3.org/2000/01/rdf-schema#label> ?label }";
    private static final String PROJECT_SPARQL_QUERY="SELECT ?label "
                                             +" WHERE{?project <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://eurocris.org/ontology/cerif#Project>. "
                                             +" ?project <http://eurocris.org/ontology/cerif#has_acronym> ?label }";
    
    private static final String PUBLICATION_SPARQL_QUERY="SELECT ?label "
                                             +" WHERE{?publication <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://eurocris.org/ontology/cerif#Publication>. "
                                             +" ?publication <http://eurocris.org/ontology/cerif#has_title> ?label }";
    
    private static final String ORGANIZATION_UNIT_SPARQL_QUERY="SELECT ?label "
                                             +" WHERE{?org <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://eurocris.org/ontology/cerif#OrganisationUnit>. "
                                             +" ?org <http://eurocris.org/ontology/cerif#has_acronym> ?label }";
    
    private static void createThesaurus(List<File> folders, String sparqlQuery, boolean useTokenizer, int minOccurence, String outputFilename) throws RepositoryException, IOException, RDFParseException, MalformedQueryException, QueryEvaluationException{
        Map<String, Integer> termsMap=new TreeMap<>();
        for(File folder : folders){
            for(File f : folder.listFiles()){
                termsMap=importData(termsMap,f, sparqlQuery, useTokenizer);
            }
        }
        Set<String> terms=findCommonTerms(termsMap, minOccurence);
        JsonArray termsArray=exportToJsonArray(terms);
        exportToFile(termsArray, outputFilename);
    }
    
    private static Map<String, Integer> importData(Map<String, Integer> termsMap,File file, String query, boolean useTokenizer) throws RepositoryException, IOException, RDFParseException, MalformedQueryException, QueryEvaluationException{
        System.out.println("File: "+file.getName());
        Repository repo=new SailRepository(new MemoryStore());
        URI context=repo.getValueFactory().createURI("http://graph");
        repo.initialize();
        RepositoryConnection repoConn=repo.getConnection();
        repoConn.add(file,"http://graph",RDFFormat.NTRIPLES, context);
        TupleQueryResult results=repoConn.prepareTupleQuery(QueryLanguage.SPARQL, query).evaluate();
        
        while(results.hasNext()){
            BindingSet result=results.next();
            String label = result.getValue("label").stringValue();
            if(useTokenizer){
                for(String splittedTerm : label .split(" ")){
                    checkElementAndAdd(termsMap, splittedTerm);
                }
            }else{
                checkElementAndAdd(termsMap, label);
            }
        }
        return termsMap;
    }

    private static void checkElementAndAdd(Map<String,Integer> termsMap, String termValue){
        if(termValue.length() > TERM_THRESHOLD && !termValue.equalsIgnoreCase(UNKNOWN_VALUE)){
            if(!termsMap.containsKey(termValue)){
                termsMap.put(termValue, 1);
            }else{
                int occurences=termsMap.get(termValue);
                termsMap.put(termValue, occurences+1);
            }
        }
    }
    
    private static Set<String> findCommonTerms(Map<String,Integer> termsMap, int threshold){
        Set<String> retSet=new HashSet<>();
        Multimap<Integer,String> sortedValuesMap=TreeMultimap.create();
        for(String key : termsMap.keySet()){
            sortedValuesMap.put(termsMap.get(key), key);
        }
        for(Integer occurence : sortedValuesMap.keySet()){
            if(occurence>=threshold){
                retSet.addAll(sortedValuesMap.get(occurence));
            }
        }
        return retSet;
    }
    
    private static JsonArray exportToJsonArray(Set<String> terms){
        int counter=1;
        JsonArray termsArray=new JsonArray();
        for(String term : terms){
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(ID_LABEL, counter++);
            jsonObject.addProperty(NAME_LABEL, term);
            termsArray.add(jsonObject);
        }
        return termsArray;
    }
    
    private static void exportToFile(JsonArray jsonArray, String jsonFileName) throws IOException{
        BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(jsonFileName), "UTF-8"));
        bw.append(jsonArray.toString());
        bw.flush();
        bw.close();
    }
    
    public static void main(String[] args) throws RepositoryException, IOException, RDFParseException, MalformedQueryException, QueryEvaluationException{
        createThesaurus(Arrays.asList(new File("path_to_folder")), PERSON_SPARQL_QUERY, true, 2, "persons-firstAndLastNames.json");
        createThesaurus(Arrays.asList(new File("path_to_folder")), PROJECT_SPARQL_QUERY, false, 1, "project-acronyms.json");
        createThesaurus(Arrays.asList(new File("path_to_folder")), ORGANIZATION_UNIT_SPARQL_QUERY, false, 1, "organizationUnits-acronyms.json");
        createThesaurus(Arrays.asList(new File("path_to_folder")), PUBLICATION_SPARQL_QUERY, true, 2, "publications-titles.json");
    }
}