package gr.forth.ics.isl.thesaurusbuilder;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Yannis Marketakis (marketak 'at' ics 'dot' forth 'dot' gr)
 */
public class ThesaurusBuilder {
    private static final String ID_LABEL="id";
    private static final String NAME_LABEL="name";
    private static final String UNKNOWN_VALUE="unknown";
    private static final int TERM_THRESHOLD=2;
    
    private void buildThesaurus(List<String> folderpaths, List<String> elementNames, boolean useTokenizer, int threshold, String jsonFileName) throws IOException{
        Map<String, Integer> termsMap=new TreeMap<>();
        for(String folderPath : folderpaths){
            for(File inputFile : new File(folderPath).listFiles()){
                Document doc=Jsoup.parse(inputFile, "UTF-8");
                for(String elementName : elementNames){
                    Elements foundElements=doc.getElementsByTag(elementName);
                    for(Element el : foundElements){
                        String termValue=el.text().trim();
                        if(useTokenizer){
                            for(String splittedTerm : termValue.split(" ")){
                                this.checkElementAndAdd(termsMap, splittedTerm);
                            }
                        }else{
                            this.checkElementAndAdd(termsMap, termValue);
                        }
                    }
                }
            }
        }
        Set<String> terms=this.findCommonTerms(termsMap, threshold);
        JsonArray termsArray=this.exportToJsonArray(terms);
        this.exportToFile(termsArray, jsonFileName);
    }
        
    private void checkElementAndAdd(Map<String,Integer> termsMap, String termValue){
        if(termValue.length() > TERM_THRESHOLD && !termValue.equalsIgnoreCase(UNKNOWN_VALUE)){
            if(!termsMap.containsKey(termValue)){
                termsMap.put(termValue, 1);
            }else{
                int occurences=termsMap.get(termValue);
                termsMap.put(termValue, occurences+1);
            }
        }
    }
    
    private Set<String> findCommonTerms(Map<String,Integer> termsMap, int threshold){
        Set<String> retSet=new HashSet<>();
        Multimap<Integer,String> sortedValuesMap=TreeMultimap.create();
        for(String key : termsMap.keySet()){
            sortedValuesMap.put(termsMap.get(key), key);
        }
        for(Integer occurence : sortedValuesMap.keySet()){
            if(occurence>=threshold){
                retSet.addAll(sortedValuesMap.get(occurence));
            }
        }
        return retSet;
    }
    
    private JsonArray exportToJsonArray(Set<String> terms){
        int counter=1;
        JsonArray termsArray=new JsonArray();
        for(String term : terms){
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(ID_LABEL, counter++);
            jsonObject.addProperty(NAME_LABEL, term);
            termsArray.add(jsonObject);
        }
        return termsArray;
    }
    
    private void exportToFile(JsonArray jsonArray, String jsonFileName) throws IOException{
        BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(jsonFileName), "UTF-8"));
        bw.append(jsonArray.toString());
        bw.flush();
        bw.close();
    }
    
    public static void main(String []args) throws IOException{
        new ThesaurusBuilder().buildThesaurus(Arrays.asList("persons-EKT","persons-RCUK"),Arrays.asList("cfFirstNames"),false,1,"persons-firstNames.json");
        new ThesaurusBuilder().buildThesaurus(Arrays.asList("persons-EKT","persons-RCUK"),Arrays.asList("cfFamilyNames"),false,2,"persons-lastNames.json");
        new ThesaurusBuilder().buildThesaurus(Arrays.asList("persons-EKT","persons-RCUK"),Arrays.asList("cfFirstNames","cfFamilyNames"),false,2,"persons-firstAndLastNames.json");
        new ThesaurusBuilder().buildThesaurus(Arrays.asList("projects-EKT","projects-RCUK"),Arrays.asList("cfAcro"),false,1,"project-acronyms.json");
        new ThesaurusBuilder().buildThesaurus(Arrays.asList("organizations-EKT","organizations-RCUK"),Arrays.asList("cfAcro"),false,1,"organizationUnits-acronyms.json");
        new ThesaurusBuilder().buildThesaurus(Arrays.asList("publications-EKT","publications-RCUK"),Arrays.asList("cfTitle"),true,2,"publications-titles.json");
    }
}