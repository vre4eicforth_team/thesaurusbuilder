# ThesaurusBuilder #

ThesaurusBuilder is a component that extracts information from data expressed in CERIF XML format and constructs a thesaurus of terms.

The purpose of the thesauri that are being constructed are for being exploited by the suggestion components that are under development in VRE4EIC.

So far ThesaurusBuilder supports constructing thesauri from the following collections:

* first names and family names from collections of Persons
* acronyms from collections of projects
* acronyms from collections of organizations
* common terms from collections containing publication titles